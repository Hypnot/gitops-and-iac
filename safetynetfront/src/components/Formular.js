import React, { useState } from 'react';
import safetyNetLogo from '../assets/img/SafetyNetInsuranceLogo.png';
import api from "../services/api";

function Formular() {
    const [firstName, setFirstname] = useState('');
    const [lastName, setName] = useState('');
    const [ahvNumber, setAhvNumber] = useState('');
    const [iban, setIban] = useState('');
    const [costEstimate, setCostEstimate] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await api.post('send-form', {
                firstName,
                lastName,
                ahvNumber,
                iban,
                costEstimate,
            });
            if (response.status === 200) {
                console.log('Data successfully send.');
            }
        } catch (error) {
            console.error('Error while sending the data', error);
        }
    };

    return (
        <div>
            <img src={safetyNetLogo} alt="SafetyNet Logo" height={320} />
            <form onSubmit={handleSubmit}>
                <label>
                    Firstname:
                    <input
                        type="text"
                        value={firstName}
                        onChange={(e) => setFirstname(e.target.value)}
                    />
                </label>
                <label>
                    Lastname:
                    <input
                        type="text"
                        value={lastName}
                        onChange={(e) => setName(e.target.value)}
                    />
                </label>
                <label>
                    AHV Number:
                    <input
                        type="text"
                        value={ahvNumber}
                        onChange={(e) => setAhvNumber(e.target.value)}
                    />
                </label>
                <label>
                    IBAN:
                    <input
                        type="text"
                        value={iban}
                        onChange={(e) => setIban(e.target.value)}
                    />
                </label>
                <label>
                    Cost Estimate:
                    <input
                        type="number"
                        value={costEstimate}
                        onChange={(e) => setCostEstimate(e.target.value)}
                    />
                </label>
                <button type="submit">Send</button>
            </form>
        </div>
    );
}

export default Formular;
