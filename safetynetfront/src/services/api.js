import axios from "axios"
const url = new URL(window.location.href);
const baseUrl = url.hostname;

export default axios.create({
    baseURL: `http://${baseUrl}:5600/api/v1`,
    timeout: 5000,
    headers: {
        "Content-Type": "application/json"
    }
})