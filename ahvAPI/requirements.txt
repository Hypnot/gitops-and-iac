asgiref==3.7.2
Django==5.0.2
pip==23.2.1
setuptools==68.1.2
sqlparse==0.4.4
tzdata==2024.1
wheel==0.41.1
daphne==4.1.0
