import json
from django.test import TestCase, Client


class VerifyTest(TestCase):
    def setUp(self):
        self.client = Client()

    def test_valid_ahv_number(self):
        data = {"ahvNumber": "756.1234.5678.90"}
        response = self.client.post('/api/verify', json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {"correct": True})

    def test_invalid_ahv_number(self):
        data = {"ahvNumber": "1234567890123"}
        response = self.client.post('/api/verify', json.dumps(data), content_type='application/json')
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {"error": "Invalid"})

    def test_get_method(self):
        response = self.client.get('/api/verify')
        self.assertEqual(response.status_code, 405)
