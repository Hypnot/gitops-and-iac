from django.urls import path
from api.views import Verify

urlpatterns = [
    path('verify', Verify.as_view(), name='verify')
]
