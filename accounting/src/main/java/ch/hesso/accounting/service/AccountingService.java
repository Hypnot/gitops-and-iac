package ch.hesso.accounting.service;

import ch.hesso.accounting.lib.RequestSender;
import ch.hesso.accounting.model.Claim;
import ch.hesso.accounting.repository.AccountingRepository;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.net.http.HttpResponse;

/**
 * @author Ivan Kostanjevec
 */
@Service
public class AccountingService {

    private final AccountingRepository repository;

    @Autowired
    public AccountingService(AccountingRepository repository) {
        this.repository = repository;
    }

    @Transactional
    public void handleClaim(Claim claim) throws ResponseStatusException {
        boolean success = repository.saveClaim(claim);
        if (!success) throw new ResponseStatusException(HttpStatusCode.valueOf(400));

        try {
            HttpResponse<String> response = RequestSender.postRequest(claim, "http://localhost:8080/api/transmit");
            if (response.statusCode() != 200) throw new ResponseStatusException(HttpStatusCode.valueOf(400));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatusCode.valueOf(400));
        }
    }
}
