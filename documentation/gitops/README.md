# Understanding GitOps
For a long time, even as we've automated much of how software gets made, setting up the actual infrastructure
(servers, networks, etc.) has been a slow, manual task needing experts to handle it. But today, infrastructure needs to be
super flexible – scaling up and down instantly to support software that's constantly being updated.

Modern applications are built to change quickly. Companies with good DevOps practices release new code many times a day!
They achieve this through careful version control, code reviews, and automated systems for testing and deployment. 

GitOps brings that same type of automation to infrastructure setup. Think of it like this: your infrastructure designs 
become code files, and changes to those files trigger the whole system to update itself. GitOps ensures everything, from
the cloud resources to the basic settings, consistently matches your design – just like how building your application code
always produces the same software.

## Principle of GitOps

- **Declaratice:** A system managed by GitOps must have its desired state expressed declaritively.
- **Versioned and Immutable:** Desired state is stored in a way that enforces immutability, versioning and retains a complete version history.
- **Pulled Automatically:** Software agents automatically pull the desired state declarations from the source.
- **Continuously Reconciled:** Software agents continuously observe actual system state and attempt to apply the desired state.

## Put GitOps into Practice

### Infrastructure as Code (IaC)

GitOps is a way to manage infrastructure where you use a Git repository to hold the
ultimate instructions for how everything should be set up. This means all the details about your infrastructure –
what servers you need, what software to install, etc. – are written down as code. Think of Git as the version control $
system that keeps track of any changes you make to these infrastructure instructions.

The beauty of GitOps is that it might not directly store every tiny detail of your system's desired state
(like the exact number of replicas needed
at a given moment). Instead, it focuses on the core configuration files that guide how your infrastructure should
behave overall. 

### Merge Request and Pull Requests (MRs)

GitOps uses merge requests (or pull requests) as the gatekeepers for any changes
to your infrastructure setup. Think of these requests like asking permission – before any change gets applied,
your team can review and discuss them. Everyone can add comments or suggest edits, and there might even be a formal
approval process. 

Once a change request is approved and merged, it becomes part of your main blueprint for the system.
This creates a clear history of exactly what changed and why, making it much easier to track things down later if needed.

### Continuous Integration/Continuous Deployment (CI/CD)

Think of GitOps as having a watchful robot assistant for your
infrastructure. It uses a process similar to how developers build and test software (CI/CD) to keep things running smoothly.

Whenever you approve a change to your infrastructure design in Git, the robot swings into action and updates your actual
systems to match. If anyone manually tinkers with the setup, or something goes wrong unexpectedly, the GitOps automation
notices and steps in to fix it. This ensures everything always stays perfectly aligned with the "official" design stored in Git.

GitLab provides handy tools to make this robot-like automation easy to set up, but other systems can also be used to do the 
same job.

## Benefits of GitOps
GitOps offers several key benefits, including improved efficiency and security across your operations. 
By automating infrastructure management and using a secure version-controlled workflow, GitOps reduces both manual 
work and the potential for errors. Developers also benefit from GitOps, as they can manage infrastructure using the
same tools and processes they're already familiar with from the software development world. This streamlining translates to
reduced operational costs and faster deployment of updates and new features.

Furthermore, GitOps allows you to manage your entire application lifecycle and infrastructure using a single, unified approach.
This brings development and operations teams closer together, fostering better collaboration and understanding.

Importantly, GitOps provides flexibility. Since your infrastructure is defined as code, it's not locked to a specific provider
or platform. This makes it significantly easier to switch cloud providers or adapt your infrastructure setup as needed. 
You can often reuse a large portion of your existing code, potentially making big changes as simple as adjusting a few 
configuration settings.

# Our Implementation
In our project we used multiple different frameworks and technologies to build our microservices. Docker was used to containerize the
different applications. In the following sections we will explain how we used Terraform in combination with Docker and GitLab CI/CD
pipelines. 

Our project looks as follows:

![Architecture](./images/Architecture.png)

## GitOps Cycle

The developers create the applications and a Docker container for each application. In the next step, the
Terraform files are created, which are responsible for configuring the infrastructure. All elements (Terraform files,
Docker containers) are then published on GitLab. In our project, the pipelines are used for automatic CI/CD.
These pipelines must be adapted if a new application is added by adding a new build - and test job. The applications are
then uploaded to Azure in a resource group via the Gitlab pipelines. In the final step, the applications are made
publicly accessible via Azure.

![GitOps Cycle](./images/GitOps Cycle.png)

## Docker
As already mentioned, we used Docker to containerize our applications. This means that every application runs in its own container.
To be able to run the different applications in containers, we need to create our own Docker images. This is done by creating a `Dockerfile`
for each microservice. These Dockerfiles have always a similar structure, it can vary a bit depending on the application. The structure:

1. We start by defining the base image. 
2. We then copy the application code into the image.
3. We then install the dependencies.
4. We then expose the port on which the application is running.
5. We then define the command to run the application.

Here you can se an example of the dockerfile for the AHV API microservice:

```Dockerfile
FROM python:3.10
LABEL authors="Ivan Kostanjevec"

COPY .. /usr/src/app/ahvAPI/

WORKDIR /usr/src/app/ahvAPI
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 1400

ENTRYPOINT ["daphne", "ahvAPI.asgi:application", "-b", "0.0.0.0", "-p", "1400"]
```
The first line defines the base image, in this case it is the python 3.10 image. We then copy the application code into the 
image and install the dependencies. We then expose the port on which the application is running and define the command to run 
the application.

Note that the entrypoint represents the command which will be run when the container is started. You need to make sure,
to define this as an array and not as a string.

In our case, images are stored in the GitLab container registry from our project. 

## Terraform
You can refer the [terraform documentation](../terraform/README.md) for more information.

## Pipeline
Our pipeline is composed of three different stages: 

### Build
In the build stage we build the Docker images for the different microservices. First the runner logs into the GitLab container registry, 
to be able to push the images to the registry. Then we build the images and set the tag to `$CI_COMMIT_BRANCH`. This means that the
tag of the image will be the name of the branch. This is useful, because in this way we can make sure that the production images won't 
be affected by the changes in the development branch. After the build, the image is pushed to the GitLab container registry.

Here you can see one example of our build jobs:

```yml
ahv-build-job:
  stage: build
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  script:
    - cd ahvAPI
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - docker build -t $CI_REGISTRY/ivan.kostanje/gitops-and-iac/ahv-api:$CI_COMMIT_BRANCH .
    - docker push $CI_REGISTRY/ivan.kostanje/gitops-and-iac/ahv-api:$CI_COMMIT_BRANCH
```

**Note that we use the "docker in docker" image (dind) this is neceseary because we need to run docker commands inside the docker container.**

### Test
Not all of our images need to be tested, that's because some of them test themselves during the build phase. The other images need to
be tested, in order to make sure that the code is working as expected. The tests are run using the already built images from the registry.
The entrypoint for the images needs to be manually specified. This is because the different frameworks need different commands to run the
tests.

Here you can see one example of our test jobs:

```yml
ahv-test-job:
  stage: test
  image: docker:20.10.16
  services:
    - docker:20.10.16-dind
  script:
    - docker run --entrypoint "/bin/sh" $CI_REGISTRY/ivan.kostanje/gitops-and-iac/ahv-api:$CI_COMMIT_BRANCH -c "python manage.py test"
```
**Note that here we also use the *dind* image**

### Deploy
In the deploy stage we use Terraform to deploy the different microservices to Azure. Because of monetary constraints this the deployment
won't always be executed. The deployment is always triggered when a new commit/merge request is pushed to the main branch. These pushes happen
rarely because we use the feature branch strategy. This way the team members can work on their own branches without affecting the production.

If you push to a feature branch, the pipeline will only build and test the images. But if you need to have a running instance of the application
of your feature branch, you can run the pipeline manually from the browser. This will trigger all the stages of the pipeline.
For each branch, a new resource group is created in Azure.

Here you can see our deploy job for the main branch:

```yml
deploy-prod-job:
  stage: deploy
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
  image: 
    name: hashicorp/terraform:1.7.4
    entrypoint: [""]
  script:
    - export TF_VAR_db_admin_username=$DB_USER TF_VAR_db_admin_password=$DB_PASS
    - terraform init -backend-config="address=$ADDRESS" -backend-config="lock_address=$LOCK_ADDRESS" -backend-config="unlock_address=$UNLOCK_ADDRESS" -backend-config="username=$USERNAME" -backend-config="password=$GITLAB_ACCESS_TOKEN" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5"
    - terraform apply -replace="azurerm_container_group.container" -auto-approve
```

And here you can see the deploy job for the feature branches:

```yml
deploy-dev-job:
  stage: deploy
  rules:
    - if: ($CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH && $CI_PIPELINE_SOURCE == "web")
  image: 
    name: hashicorp/terraform:1.7.4
    entrypoint: [""]
  script:
    - export TF_VAR_db_admin_username=$DB_USER TF_VAR_db_admin_password=$DB_PASS
    - terraform init -backend-config="address=$ADDRESS" -backend-config="lock_address=$LOCK_ADDRESS" -backend-config="unlock_address=$UNLOCK_ADDRESS" -backend-config="username=$USERNAME" -backend-config="password=$GITLAB_ACCESS_TOKEN" -backend-config="lock_method=POST" -backend-config="unlock_method=DELETE" -backend-config="retry_wait_min=5"
    - terraform apply -replace="azurerm_container_group.container" -var branch="$CI_COMMIT_BRANCH" -auto-approve
```

The job uses the Terraform image. Two different environment variables are set, this is because these variables contain sensitive information.
And can therefore not be hardcoded in Terraform. Then we initialize Terraform with the backend configuration and apply the changes. 

**Note that in the dev job we set the variable `branch` to `$CI_COMMIT_BRANCH` this is because we need to create a new resource group for each branch.**

**Note that we use the `replace` flag, this is because we need to replace the container group, as Terraform doesn't update the containers if 
the image changes.**