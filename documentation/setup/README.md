# Setup

## Dependencies

- Docker
- Azure CLI
- Azure subscription

## Introduction

In this guide, you will learn how you can use the existing Terraform project and deploy it on Azure with automated
testing and deployment. If you want to learn what the different parts of the Terraform code are doing, you will need
to check out the [terraform documentation](../terraform/README.md).

## Terraform installation

The first thing that needs to be done is to install Terraform on your local machine, to do this you will need
to [download](https://developer.hashicorp.com/terraform/install) the correct binary for your system.
This binary then needs to be added to your PATH.
(If you are on Windows, you need to download the AMD64 binary this is not only for AMD systems but generally for
64-bit systems)

**DISCLAIMER: This project was made using Terraform 1.7.4. your experience might differ with other versions.**

## Project Fork

After you install Terraform, you can fork this project.
This project uses many different GitLab features which are not available to guests of this project.
This includes the GitLab Container Registry, Terraform state management and CI/CD pipelines for testing and deployment.

## Azure setup

**DISCLAIMER: All code shown below was tested in powershell if nothing is annotated.
It is not guaranteed that it will work in any other terminal.**

For Terraform to be able to deploy something on your Azure subscription, you will need to create a service principal
that Terraform can use.
To do this, you will need to use the [Azure CLI](https://learn.microsoft.com/en-us/cli/azure/install-azure-cli).
If you are not already logged in, you can use the following command.

```powershell
az login
```

To create the service principal, you will need to execute the following command. You need to make sure that you put
your subscription id into the command.

```powershell
az ad sp create-for-rbac --role="Contributor" --scopes="/subscriptions/<SUBSCRIPTION_ID>"
```

This should give you an output which looks like this.

```
Creating 'Contributor' role assignment under scope '/subscriptions/35akss-subscription-id'
The output includes credentials that you must protect. Be sure that you do not include these credentials in your code or check the credentials into your source control. For more information, see https://aka.ms/azadsp-cli
{
  "appId": "xxxxxx-xxx-xxxx-xxxx-xxxxxxxxxx",
  "displayName": "azure-cli-2022-xxxx",
  "password": "xxxxxx~xxxxxx~xxxxx",
  "tenant": "xxxxx-xxxx-xxxxx-xxxx-xxxxx"
}
```

If you want to be able to run the deployment manually from your local machine, you will need to create some environment
variables.
Keep in mind that those values will be lost when your powershell session ends.
Make sure to write the variables down somewhere as they will be needed later in the setup.

To add the environment variables in powershell, you need to set them as follows.

```powershell
$Env:ARM_CLIENT_ID = "<APPID_VALUE>"
$Env:ARM_CLIENT_SECRET = "<PASSWORD_VALUE>"
$Env:ARM_SUBSCRIPTION_ID = "<SUBSCRIPTION_ID>"
$Env:ARM_TENANT_ID = "<TENANT_VALUE>"
```

## GitLab setup

### GitLab access token

The state will be saved in your GitLab project, and for Terraform to be able to access this state, it needs some API
permissions.
For this, you will need to create a GitLab access token with the "api" scope.
To create one, follow these steps:

1. Click on GitLab on your avatar
2. Select **Edit profile**
3. In your user settings, select **Access Tokens**
4. On the top right of the box, click on **Add new token**
5. Set a name and expiration date
6. In the scopes section select **api**
7. Click on **Create personal access token**

After the token creation, you will be able to copy the token.
You need to do this now, as there is no way to access this token again.
So copy the token and keep it for later use in this guide.

### Terraform state

To tell Terraform that you are using GitLab as a backend to store your state, you will need to use a special
*terraform init* command.
To get this command, go to your GitLab project and select **Terraform states** from the **Operate** menu.
If there is already a state called *default*, delete it.
Then click on **Copy Terraform init command**.

The command should look something like this.

```shell
export GITLAB_ACCESS_TOKEN=<YOUR-ACCESS-TOKEN>
export TF_STATE_NAME=default
terraform init \
    -backend-config="address=<PROJECT>/terraform/state/$TF_STATE_NAME" \
    -backend-config="lock_address=<PROJECT>/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="unlock_address=<PROJECT>/terraform/state/$TF_STATE_NAME/lock" \
    -backend-config="username=<USER_NAME>" \
    -backend-config="password=$GITLAB_ACCESS_TOKEN" \
    -backend-config="lock_method=POST" \
    -backend-config="unlock_method=DELETE" \
    -backend-config="retry_wait_min=5"
```

This is a shell command. If you are working with shell, you can paste this command as it is in your terminal. But
if you are using powershell, you need to modify it a bit. It should look like this.

```powershell
$Env:GITLAB_ACCESS_TOKEN = <YOUR-ACCESS-TOKEN>
$Env:TF_STATE_NAME = "default"
terraform init `
-backend-config="address=<PROJECT>/terraform/state/$Env:TF_STATE_NAME" `
-backend-config="lock_address=<PROJECT>/terraform/state/$Env:TF_STATE_NAME/lock" `
-backend-config="unlock_address=<PROJECT>/terraform/state/$Env:TF_STATE_NAME/lock" `
-backend-config="username=<USER_NAME>" `
-backend-config="password=$Env:GITLAB_ACCESS_TOKEN" `
-backend-config="lock_method=POST" `
-backend-config="unlock_method=DELETE" `
-backend-config="retry_wait_min=5"
```

Alternatively, you could also set these parameters in the http backend section. This would look something like this

```terraform
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.0.2"
    }
  }
  backend "http" {
    address        = "<ADDRESS>"
    lock_address   = "<LOCK_ADDRESS>"
    unlock_address = "<UNLOCK_ADDRESS>"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }

  required_version = ">= 1.1.0"
}
```

This solution would work, but if you want to exchange your backend in the future or use different backends for
development and production, it would be harder. We also recommend that you at least set the username and password
configurations in the terminal as they would otherwise be visible in plain text. In the remainder of this guide, we are
assuming that you used the first method with the configuration in the terminal.

### Variables

At this stage, you could use Terraform locally with no problem, but to be able to automate the testing and deployment,
we are using GitLab CI/CD pipelines. Logically, the pipeline runner needs the same access rights to the different
resources that you also need to be able to use Terraform. To do this, you will need to set some GitLab variables.
They can be used in your CI/CD pipelines. You don't need to set up the pipeline, as it was already done.

To set some GitLab variables, you need to follow these steps.

1. Go to your GitLab project
2. Click on **Settings**
3. Click on **CI/CD**
4. Press on the **Expand** button under the **Variables** section
5. To add a variable, press on **Add variable**

You need to set the following variables, sorted alphabetically.

| Variable Name       | Value                                                                                      |   Flag   |
|---------------------|:-------------------------------------------------------------------------------------------|:--------:|
| ADDRESS             | https<nolink>://gitlab.com/api/v4/projects/`<PROJECT>`/terraform/state/$TF_STATE_NAME      | Expanded |
| ARM_CLIENT_ID       | <YOUR_ARM_CLIENT_ID>                                                                       |  Masked  |
| ARM_CLIENT_SECRET   | <YOUR_ARM_CLIENT_SECRET>                                                                   |  Masked  |
| ARM_SUBSCRIPTION_ID | <YOUR_ARM_SUBSCRIPTION_ID>                                                                 |  Masked  |
| ARM_TENANT_ID       | <YOUR_ARM_TENANT_ID>                                                                       |  Masked  |
| CONTAINER_REGISTRY  | <PROJECT_PATH> (ex. ivan.kostanje/gitops-and-iac)                                          |          |
| DB_PASS             | <YOUR_DATABASE_ADMIN_PASSWORD>                                                             |  Masked  |
| DB_USER             | <YOUR_DATABASE_ADMIN_USERNAME>                                                             |  Masked  |
| GITLAB_ACCESS_TOKEN | <YOUR_GITLAB_ACCESS_TOKEN>                                                                 |  Masked  |
| LOCK_ADDRESS        | https<nolink>://gitlab.com/api/v4/projects/`<PROJECT>`/terraform/state/$TF_STATE_NAME/lock | Expanded |
| TF_STATE_NAME       | $CI_COMMIT_BRANCH                                                                          | Expanded |
| UNLOCK_ADDRESS      | https<nolink>://gitlab.com/api/v4/projects/`<PROJECT>`/terraform/state/$TF_STATE_NAME/lock | Expanded |
| USERNAME            | <YOUR_GITLAB_USERNAME> (Use the one for which the token was generated)                     |  Masked  |

## Conclusion

Everything is now setup correctly and can be used. Whenever you change something in your code. It will be built, tested
and deployed. But this is normal behavior in most CI/CD pipelines. But with Terraform, you can change the
infrastructure by changing some code. This infrastructure will be automatically built on Azure and deployed.

If you want to learn how to use Terraform,
then you should check out the [terraform documentation](../terraform/README.md).

And if you don't understand what GitOps is, or you want to know how we implemented it,
then you should check out the [GitOps documentation](../gitops/README.md).