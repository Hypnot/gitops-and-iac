variable "location" {
  description = "The location/region where the Azure Resource Group should be created."
  default     = "switzerlandnorth"
}

variable "cpu" {
  description = "Default CPU for containers"
  default     = 0.5
}

variable "memory" {
  description = "Default memory for containers"
  default     = 0.5
}

variable "container_registry" {
  description = "Container registry for the GitLab project"
}

variable "branch" {
  description = "AHV Image location"
  default = "main"
}

variable "db_admin_username" {
  description = "Admin username of the entire SQL server"
  sensitive = true
}

variable "db_admin_password" {
  description = "Password for the SQL server admin"
  sensitive = true
}

variable "user" {
  description = "GitLab username"
}