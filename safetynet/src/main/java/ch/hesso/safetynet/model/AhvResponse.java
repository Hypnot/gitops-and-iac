package ch.hesso.safetynet.model;

/**
 * @author Ivan Kostanjevec
 */
public record AhvResponse(boolean correct) {
}
