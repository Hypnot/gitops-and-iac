package ch.hesso.safetynet.model;

/**
 * @author Ivan Kostanjevec
 */
public class Claim {

    private final int amount;
    private final String iban;

    public Claim(int amount, String iban) {
        this.amount = amount;
        this.iban = iban;
    }

    @Override
    public String toString() {
        return "{\"amount\":" + amount + ",\"iban\":\"" + iban + "\"}";
    }
}
