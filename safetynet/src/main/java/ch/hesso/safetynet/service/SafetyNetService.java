package ch.hesso.safetynet.service;

import ch.hesso.safetynet.lib.RequestSender;
import ch.hesso.safetynet.model.*;
import ch.hesso.safetynet.repository.SafetyNetRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatusCode;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.net.http.HttpResponse;

/**
 * @author Ivan Kostanjevec
 */
@Service
public class SafetyNetService {

    private final SafetyNetRepository repository;

    @Autowired
    public SafetyNetService(SafetyNetRepository repository) {
        this.repository = repository;
    }

    public void handleInsuranceClaim(InsuranceClaim claim) {
        Customer customer = new Customer(claim.firstName(), claim.lastName(), claim.iban(), claim.ahvNumber());
        repository.saveCustomer(customer);

        try {
            HttpResponse<String> ahvResponse = RequestSender.postRequest(new AhvNumber(claim.ahvNumber()), "http://localhost:1400/api/verify");
            if (ahvResponse.statusCode() != 200) throw new ResponseStatusException(HttpStatusCode.valueOf(400));

            ObjectMapper mapper = new ObjectMapper();
            AhvResponse ahvResponseObject = mapper.readValue(ahvResponse.body(), AhvResponse.class);
            if (!ahvResponseObject.correct()) throw new ResponseStatusException(HttpStatusCode.valueOf(400));

            HttpResponse<String> accountingResponse = RequestSender.postRequest(new Claim(claim.costEstimate(), claim.iban()), "http://localhost:2500/api/v2/claim");
            if (accountingResponse.statusCode() != 200) throw new ResponseStatusException(HttpStatusCode.valueOf(400));
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatusCode.valueOf(400));
        }
    }
}
