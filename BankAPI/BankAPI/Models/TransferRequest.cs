﻿namespace BankAPI.Models;

public class TransferRequest
{
    public double Amount { get; set; }
    public string IBAN { get; set; }
}